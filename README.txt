CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------
E-MAILiT provides a free, creative solution which allows publishers to utilize 
influential features straightforwardly into their sites by means of a completely
customizable environment. The E-MAILiT service smartly combines a mixture of 
share buttons, media solutions, analytics, and much more, empowering publishers 
to build site hits, helping client engagement, enabling simple sharing and 
promoting of their content, by focusing on their sharers.

E-MAILiT is a powerful social sharing platform and an awesome marketing web tool
for your website. You can find out much more in our website 
https://www.e-mailit.com (demo)

REQUIREMENTS
------------
None

INSTALLATION
------------
1. Install the module
2. Check permissions on admin/user/access to decide which role and which node 
type is provided with the button.
3. Go to modules->E-MAILiT Share Buttons Configure, and save configuration even
if you do not make any changes (IMPORTANT).

CONFIGURATION
-------------
1. Go to modules->E-MAILiT Share Buttons Configure, and configure your E-MAILiT 
Share Buttons as desired
2. Enjoy

MAINTAINERS
-----------
E-MAILiT (https://www.e-mailit.com)
