<?php

/**
 * @file
 * Administration settings form area for EMAILiT module.
 */

/**
 * Form for settings for the  EMAILiT module.
 */
function emailit_admin_settings($form, &$form_state) {
  $form['content']['#attached']['js'][] = array(
    'data' => (drupal_get_path('module', 'emailit') . '/js/emailit.admin.js'),
    'scope' => 'header',
    'type' => 'file',
  );
  $form['content']['#attached']['css'] = array(
    'data' => (drupal_get_path('module', 'emailit') . '/emailit.admin.css'),
  );

  $form['emailit_customize_buttons'] = array(
    '#type' => 'fieldset',
    '#title' => t('Customize your buttons'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['emailit_customize_buttons']['emailit_toolbar_type'] = array(
    '#type' => 'radios',
    '#title' => t('STYLE'),
    '#options' => array(
      'large' => t('Large'),
      'small' => t('Small'),
      'native' => t('Native (original 3rd party share buttons - note that the code for these plugins are native to the social network and thus have limited options for modification)'),
    ),
    '#default_value' => variable_get('emailit_toolbar_type', 'large'),
  );
  $form['emailit_customize_buttons']['emailit_back_color'] = array(
    '#type' => 'textfield',
    '#title' => t('BACKGROUND COLOR (leave it blank for default style)'),
    '#default_value' => variable_get('emailit_back_color', ''),
    '#size' => 5,
    '#attributes' => array(
      'class' => array('colorInput'),
    ),
  );
  $form['emailit_customize_buttons']['emailit_display_counter'] = array(
    '#type' => 'checkbox',
    '#title' => t('DISPLAY COUNTERS'),
    '#default_value' => variable_get('emailit_display_counter', 0),
  );
  $form['emailit_customize_buttons']['emailit_circular'] = array(
    '#type' => 'checkbox',
    '#title' => t('CIRCLE ICON'),
    '#default_value' => variable_get('emailit_circular', 0),
    '#states' => array(
      'visible' => array(
        ':input[name="emailit_toolbar_type"]' => array(
                    array('value' => 'large'),
                    array('value' => 'small'),
        ),
      ),
    ),
  );

  $form['emailit_global_button_options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Global button (more sharing options)'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['emailit_global_button_options']['emailit_global_button'] = array(
    '#type' => 'radios',
    '#title' => t('ORDER'),
    '#options' => array(
      'last' => t('Show last in sharing toolbar'),
      'first' => t('Show last in sharing toolbar'),
      'disabled' => t('Deactivate'),
    ),
    '#default_value' => variable_get('emailit_global_button', 'last'),
  );
  $form['emailit_global_button_options']['emailit_open_on'] = array(
    '#type' => 'radios',
    '#title' => t('OPEN GLOBAL SHARING MENU ON'),
    '#options' => array(
      'onclick' => t('Click'),
      'onmouseover' => t('Hover'),
    ),
    '#default_value' => variable_get('emailit_open_on', 'onclick'),
  );
  $form['emailit_global_button_options']['emailit_text_display'] = array(
    '#type' => 'textfield',
    '#title' => t('SHARE TEXT'),
    '#default_value' => variable_get('emailit_text_display', 'Share'),
    '#size' => 5,
    '#states' => array(
      'visible' => array(
        ':input[name="emailit_toolbar_type"]' => array('value' => 'native'),
      ),
    ),
  );
  $form['emailit_global_button_options']['emailit_text_color'] = array(
    '#type' => 'textfield',
    '#title' => t('TEXT COLOR (leave it blank for default style)'),
    '#default_value' => variable_get('emailit_text_color', ''),
    '#size' => 5,
    '#states' => array(
      'visible' => array(
        ':input[name="emailit_toolbar_type"]' => array('value' => 'native'),
      ),
    ),
    '#attributes' => array(
      'class' => array('colorInput'),
    ),
  );
  $form['emailit_global_button_options']['emailit_auto_popup'] = array(
    '#type' => 'textfield',
    '#title' => t('AUTO SHOW SHARE OVERLAY AFTER'),
    '#description' => t('Number in seconds'),
    '#default_value' => variable_get('emailit_auto_popup', '0'),
    '#size' => 5,
  );
  $form['emailit_floating'] = array(
    '#type' => 'fieldset',
    '#title' => t('Floating'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['emailit_floating']['emailit_floating_bar'] = array(
    '#type' => 'radios',
    '#title' => t('SHARE SIDEBAR'),
    '#options' => array(
      'disabled' => t('Deactivate'),
      'left' => t('Left'),
      'right' => t('Right'),
    ),
    '#default_value' => variable_get('emailit_floating_bar', 'disabled'),
  );
  $form['emailit_mobile'] = array(
    '#type' => 'fieldset',
    '#title' => t('Mobile Sharing'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['emailit_mobile']['emailit_mobile_bar'] = array(
    '#type' => 'checkbox',
    '#title' => t('MOBILE SHARE BAR (<a href="http://blog.e-mailit.com/2016/02/amazing-free-mobile-share-buttons.html" target="_blank">what\'s this?</a>)'),
    '#default_value' => variable_get('emailit_mobile_bar', 1),
  );
  $form['emailit_mobile']['emailit_mobile_back_color'] = array(
    '#type' => 'textfield',
    '#title' => t('BACKGROUND COLOR (leave it blank for default style)'),
    '#default_value' => variable_get('emailit_mobile_back_color', ''),
    '#size' => 5,
    '#attributes' => array(
      'class' => array('colorInput'),
    ),
  );
  $form['emailit_mobile']['emailit_mob_button_set'] = array(
    '#type' => 'radios',
    '#title' => t('SERVICES'),
    '#options' => array(
      'mob_default' => t('Default for Mobile'),
      'mob_same' => t('Same as Desktop (Standalone)'),
      'mob_custom' => t('Select your own services'),
    ),
    '#default_value' => variable_get('emailit_mob_button_set', 'mob_default'),
  );
  $form['emailit_mobile']['emailit_mobile_services'] = array(
    '#type' => 'textarea',
    '#title' => t('Mobile Services [ <a href="https://www.e-mailit.com/services" target="_blank"><strong>Service Codes</strong></a> ]'),
    '#description' => t('Separate multiple services correctly (case sensitive) by comma'),
    '#default_value' => variable_get('emailit_mobile_services', ''),
    '#wysiwyg' => FALSE,
    '#cols' => 10,
    '#states' => array(
            // Show only if placement in "content" selected.
      'visible' => array(
        ':input[name="emailit_mob_button_set"]' => array('value' => 'mob_custom'),
      ),
    ),
  );
  $form['emailit_standalone_services'] = array(
    '#type' => 'fieldset',
    '#title' => t('Standalone services'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['emailit_standalone_services']['emailit_default_buttons'] = array(
    '#type' => 'textarea',
    '#title' => t('Services [ <a href="https://www.e-mailit.com/services" target="_blank"><strong>Service Codes</strong></a> ]'),
    '#description' => t('Separate multiple services correctly (case sensitive) by comma'),
    '#default_value' => variable_get('emailit_default_buttons', 'Facebook,Twitter,Pinterest,LinkedIn'),
    '#wysiwyg' => FALSE,
    '#cols' => 10,
  );
  $form['emailit_standalone_services']['emailit_TwitterID'] = array(
    '#type' => 'textfield',
    '#title' => t('TWEET VIA (your Twitter username)'),
    '#default_value' => variable_get('emailit_TwitterID', ''),
    '#size' => 10,
  );
  $form['emailit_standalone_services']['emailit_hover_pinit'] = array(
    '#type' => 'checkbox',
    '#title' => t('PINTEREST SHAREABLE IMAGES'),
    '#default_value' => variable_get('emailit_hover_pinit', 0),
  );

  $form['emailit_placement_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Placement'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['emailit_placement_settings']['emailit_nodetypes'] = array(
    '#type' => 'checkboxes',
    '#title' => t('NODE TYPES'),
    '#description' => t('Display buttons for these node types.'),
    '#default_value' => variable_get('emailit_nodetypes', array('page', 'article')),
    '#options' => node_type_get_names(),
  );
  $form['emailit_placement_settings']['emailit_display_in_teasers'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display for node <strong>teasers</strong>'),
    '#default_value' => variable_get('emailit_display_in_teasers', '1'),
    '#description' => t('Display buttons for node <strong>teasers</strong> in selected sections.'),
    '#states' => array(
            // Disable if no section placement is selected.
      'disabled' => array(
        ':input[name="emailit_display_in_nodecont"]' => array('checked' => FALSE),
        ':input[name="emailit_display_in_nodelink"]' => array('checked' => FALSE),
      ),
    ),
  );
  $form['emailit_placement_settings']['emailit_display_in_nodelink'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display in <strong>link</strong> section'),
    '#default_value' => variable_get('emailit_display_in_nodelink', '1'),
    '#description' => t('Display buttons in the <strong>link</strong> section of node pages.'),
  );
  $form['emailit_placement_settings']['emailit_display_in_nodecont'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display in <strong>content</strong> section'),
    '#default_value' => variable_get('emailit_display_in_nodecont', '0'),
    '#description' => t('Display buttons in the <strong>content</strong> section of node pages.'),
  );
  $form['emailit_placement_settings']['emailit_display_in_rss'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display in <strong>RSS</strong> feed'),
    '#default_value' => variable_get('emailit_display_in_rss', '1'),
    '#description' => t('Display buttons in the RSS feed.'),
  );
  $form['emailit_placement_settings']['emailit_display_weight'] = array(
    '#type' => 'weight',
    '#title' => t('CONTENT WEIGHT'),
    '#default_value' => variable_get('emailit_display_weight', 40),
    '#delta' => 50,
    '#description' => t('Optional weight value for buttons displayed in the <strong>content</strong> section.'),
    '#states' => array(
            // Show only if placement in "content" selected.
      'visible' => array(
        ':input[name="emailit_display_in_nodecont"]' => array('checked' => TRUE),
      ),
    ),
  );

  $form['emailit_advanced'] = array(
    '#type' => 'fieldset',
    '#title' => t('ADVANCED OPTIONS'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['emailit_advanced']['emailit_logo'] = array(
    '#type' => 'textfield',
    '#title' => t('ADD YOUR LOGO (<a href="http://blog.e-mailit.com/2015/12/new-feature-update-add-your-logo.html" target="_blank">what\'s this?</a>)'),
    '#description' => t('Insert your Image Unit location'),
    '#default_value' => variable_get('emailit_logo', ''),
    '#size' => 30,
    '#attributes' => array(
      'placeholder' => array(t('http://')),
    ),
  );
  $form['emailit_advanced']['emailit_after_share_dialog'] = array(
    '#type' => 'checkbox',
    '#title' => t('AFTER SHARE PROMO (<a href="http://blog.e-mailit.com/2015/12/monetize-your-content-marketing.html" target="_blank">what\'s this?</a>)'),
    '#default_value' => variable_get('emailit_after_share_dialog', '1'),
  );
  $form['emailit_advanced']['emailit_thanks_message'] = array(
    '#type' => 'textfield',
    '#title' => t('AFTER SHARE PROMO HEADING'),
    '#default_value' => variable_get('emailit_thanks_message', 'Thanks for sharing!'),
    '#size' => 30,
    '#attributes' => array(
      'placeholder' => array(t('Thanks for sharing!')),
    ),
  );
  $form['emailit_advanced']['emailit_follow_services'] = array(
    '#type' => 'textfield',
    '#title' => t('FOLLOW SERVICES (show on After Share Promo)'),
    '#description' => '<ul id="social_services_follow" class="large"></ul>',
    '#default_value' => variable_get('emailit_follow_services', ''),
    '#size' => 30,
    '#attributes' => array(
      'style' => array('display:none'),
    ),
  );
  $form['emailit_advanced']['emailit_display_ads'] = array(
    '#type' => 'checkbox',
    '#title' => t('DISPLAY ADVERTS'),
    '#default_value' => variable_get('emailit_display_ads', '1'),
  );
  $form['emailit_advanced']['emailit_ad_url'] = array(
    '#type' => 'textfield',
    '#title' => t('MONETIZE (show on After Share Promo)'),
    '#description' => t('Insert your Ad Unit (or Promo) location (<a href="https://www.e-mailit.com/sample/ad-unit.html" target="_blank">Download/Save &amp; edit the sample file</a>)'),
    '#default_value' => variable_get('emailit_ad_url', ''),
    '#size' => 30,
    '#attributes' => array(
      'placeholder' => array(t('http://')),
    ),
  );
  $form['emailit_advanced']['emailit_branding'] = array(
    '#type' => 'checkbox',
    '#title' => t('E-MAILiT branding'),
    '#default_value' => variable_get('emailit_branding', '1'),
  );
  return system_settings_form($form);
}
